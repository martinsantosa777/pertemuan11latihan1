package comebackisreal.com.pertemuan11latihan1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {

    var myFirstRun :FirstRunSharePref?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //buat objek sharepref yang berisi datanya sudah diisi atau belum
        myFirstRun = FirstRunSharePref(this)

        //jika ini adalah first run
        if(myFirstRun!!.firstRun){
            val secondIntent = Intent(this,PreLoad::class.java)
            startActivity(secondIntent)
        }

        //jalankan update adapter
        updateAdapter()

        btnAddHapus.setOnClickListener {
            txtHapus.text = txtHapus.text.toString() + "\n${spinner1.selectedItem.toString()}"
        }

        btnRunHapus.setOnClickListener {
            if(txtHapus.text.isNullOrEmpty() || txtHapus.text.isNullOrBlank()){
                Toast.makeText(this, "Tidak ada data untuk dihapus", Toast.LENGTH_LONG).show()
            }
            else{
                deleteData()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        updateAdapter()
    }

    private fun updateAdapter() {
        //lakukan secara async
        doAsync {
            //ambil list nama user
            var nameList = UserTransaction(this@MainActivity).viewAllName().toTypedArray()
            uiThread {
                //jika spinner tidak null dan name list ada isisnya
                if (spinner1 != null && nameList.size != 0) {
                    //update isiynya
                    val arrayAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, nameList)
                    spinner1.adapter = arrayAdapter
                }
            }
        }
    }

    private fun deleteData(){
        doAsync {
            var nameList = UserTransaction(this@MainActivity).deleteAll(txtHapus.text.toString().lines())
            uiThread {
                updateAdapter()
                txtHapus.text=""
                Toast.makeText(this@MainActivity, "Proses delete selesai", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

