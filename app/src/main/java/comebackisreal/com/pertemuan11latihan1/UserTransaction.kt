package comebackisreal.com.pertemuan11latihan1

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import comebackisreal.com.pertemuan11latihan1.myDB.userDB

class UserTransaction(context: Context) {
    private val myDBHelper = myDBHelper(context)

    private val dbwirte = myDBHelper.writableDatabase

    fun viewAllName():List<String>{
        //buat arraylist untuk tampung nama
        val nameList:ArrayList<String> = ArrayList<String>()
        //query nama
        val selectQuery = "SELECT  ${userDB.userTable.COLUMN_NAME}" + " FROM ${userDB.userTable.TABLE_USER}"
        //ambil db
        val db = myDBHelper.readableDatabase

        //pakai cursor coba baca hasil query dan tampung kedalam cursor
        var cursor: Cursor
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var userName: String
        //cek kursor kosong atua g
        if (cursor.moveToFirst()) {
            //perulangan dari cursotr pertama karena diatas udh moveToFrst()
            do {
                //ambil namanya dari object cursor karena sebelumnya udh disimpan disana
                userName = cursor.getString(cursor.getColumnIndex(userDB.userTable.COLUMN_NAME))
                //tambahkan kedalam list
                nameList.add(userName)
            } while (cursor.moveToNext()) //ulang terus selama moveToNext() belum null artinya masih ada baris berikutnya
        }

        return nameList
    }

    fun addUser(user: User):Long{
        val db = myDBHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(userDB.userTable.COLUMN_NAME, user.nama)
        contentValues.put(userDB.userTable.COLUMN_EMAIL, user.email)
        contentValues.put(userDB.userTable.COLUMN_PHONE,user.no_hp )
        val success = db.insert(userDB.userTable.TABLE_USER,
            null, contentValues)
        db.close()
        return success
    }

    fun beginUserTransaction()
    {
        dbwirte.beginTransaction()
    }

    fun successUserTransaction()
    {
        dbwirte.setTransactionSuccessful()
    }

    fun endUserTransaction()
    {
        dbwirte.endTransaction()
    }

    fun addUserTransaction(user: User):Unit{
        val sqlString = "INSERT INTO ${userDB.userTable.TABLE_USER} " +
                "(${userDB.userTable.COLUMN_NAME}" +
                ",${userDB.userTable.COLUMN_EMAIL}" +
                ",${userDB.userTable.COLUMN_PHONE}) VALUES (?,?,?)"
        val myStatement = dbwirte.compileStatement(sqlString)
        myStatement.bindString(1,user.nama)
        myStatement.bindString(2,user.email)
        myStatement.bindString(3,user.no_hp)
        myStatement.execute()
        myStatement.clearBindings()
    }

    fun deleteAll(daftarDelete : List<String>) {
        val db = myDBHelper.writableDatabase

        daftarDelete.forEach {
            if(it.isNullOrEmpty() == false){
                val contentValues = ContentValues()
                val success = db.delete(userDB.userTable.TABLE_USER, userDB.userTable.COLUMN_NAME+" = ?", arrayOf(it))
            }
        }

        db.close()
    }
}